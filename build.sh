# rust
wasm-pack build --target web --no-typescript --dev
mv --force pkg/*.wasm public/main.wasm
mv --force pkg/*.js public/bindings.js
rm -rf pkg

# nim
nimble js -o:public/main.js src/editsc.nim

elm make src/Editsc.elm --output=public/elm.js
