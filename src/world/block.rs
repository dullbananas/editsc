#[derive(Clone, Copy)]
pub struct Block {
    bits: u32,
}


impl Block {
    pub fn from_bits(bits: u32) -> Block {
        Block {
            bits: bits,
        }
    }
}
