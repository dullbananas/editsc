module Editsc.Ui exposing
    ( section
    , Button
    , button
    , FilesInput
    , filesInput
    , row
    , text
    )

import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html
import Html.Attributes
import Html.Events
import Json.Decode


section : List (Element msg) -> Element msg
section =
    Element.column
        [ Background.color behindWhite
        , Border.rounded 18
        , Element.padding 14
        , Element.spacing 14
        , Element.width (Element.fill)
        , Font.family
            -- https://furbo.org/2018/03/28/system-fonts-in-css/
            -- https://infinnie.github.io/blog/2017/systemui.html
            -- System fonts
            [ Font.typeface "-apple-system"
            , Font.typeface "Oxygen"
            , Font.typeface "Cantarell"
            , Font.typeface "Ubuntu"
            , Font.typeface "Helvetica Neue"
            , Font.sansSerif
            ]
        ]


type alias Button msg =
    { onPress : msg
    , label : Element msg
    }


baseButton : List (Element.Attribute msg)
baseButton =
    [ Border.rounded 6
    , Border.shadow
        { offset = (0, 3)
        , size = 0
        , blur = 6
        , color = Element.rgba 0 0 0 0.15
        }
    , Background.color white
    , Element.paddingXY 6 4
    ]


button : Button msg -> Element msg
button {onPress, label} =
    Input.button baseButton
        { onPress = Just onPress
        , label = label
        }


type alias FilesInput msg =
    { onSelect : List (Json.Decode.Value) -> msg
    , label : Element msg
    }


filesInput : FilesInput msg -> Element msg
filesInput {onSelect, label} =
    Element.el
        ( baseButton ++
        -- Invisible input element in front of a button-styled element
        -- https://www.webslake.com/article/pure-css-solution-for-styling-input-type-file/
        [ Element.inFront <|
            Element.el
                [ Element.width Element.fill
                , Element.height Element.fill
                , Element.transparent True
                ] <| Element.html <|
                    Html.input
                        [ Html.Attributes.type_ "file"
                        -- Fill up entire button
                        , Html.Attributes.style "position" "absolute"
                        , Html.Attributes.style "top" "0"
                        , Html.Attributes.style "right" "0"
                        , Html.Attributes.style "bottom" "0"
                        , Html.Attributes.style "left" "0"
                        , Html.Attributes.style "margin" "0"
                        -- Triggered when files are selected
                        , Html.Events.on "change"
                            (decodeFilesSelection onSelect)
                        ] []
        ])
        label


decodeFilesSelection : (List Json.Decode.Value -> msg) -> Json.Decode.Decoder msg
decodeFilesSelection msgWrapper =
    -- currentTarget gets the input element
    Json.Decode.at ["currentTarget", "files"] decodeFileList
        |> Json.Decode.map msgWrapper


decodeFileList : Json.Decode.Decoder (List Json.Decode.Value)
decodeFileList =
    -- Get the FileList's length
    Json.Decode.field "length" Json.Decode.int
        |> Json.Decode.andThen
            ( \length ->
                -- Get all indexes
                List.range (0) (length - 1)
                    -- Create a Decoder for each index
                    |> List.map
                        ( \index -> Json.Decode.field
                            (String.fromInt index)
                            (Json.Decode.value)
                        )
                    -- Combine Decoders
                    |> List.foldl 
                        (Json.Decode.map2 (::))
                        (Json.Decode.succeed [])
            )


row : List (Element msg) -> Element msg
row =
    Element.row
        [ Element.width Element.fill
        , Element.spacing 6
        ]


text : String -> Element msg
text =
    Element.el
        [ Element.width (Element.fill)
        , Font.medium
        ] << Element.text


white : Element.Color
white =
    Element.rgb
        1 1 1


behindWhite : Element.Color
behindWhite =
    Element.rgb
        0.95 0.95 0.95
