module Editsc.Viewport exposing
    ( Viewport
    , default
    , get
    )

import Browser.Dom
import Task exposing (Task)


type alias Viewport =
    { width : Int
    , height : Int
    }


default : Viewport
default =
    { width = 0
    , height = 0
    }


get : Task x Viewport
get =
    Browser.Dom.getViewport
        |> Task.map
            ( \{viewport} ->
                { width = Basics.floor viewport.width
                , height = Basics.floor viewport.height
                }
            )
