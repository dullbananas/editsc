port module Editsc.Port exposing
    ( importWorld
    , subscription
    , Event(..)
    )

import Json.Decode as Decoder exposing (Decoder)
import Json.Encode as Value exposing (Value)

port rawSend : Value -> Cmd msg
port rawSubscribe : (Value -> msg) -> Sub msg


send : Int -> List (String, Value) -> Cmd msg
send kind =
    (::) ("kind", Value.int kind) >> Value.object >> rawSend


importWorld : Value -> Cmd msg
importWorld file =
    send 0
        [ ("file", file)
        ]


type Event
    = ErrorPopup String


subscription : (Result Decoder.Error Event -> msg) -> Sub msg
subscription toMsg =
    rawSubscribe
        (Decoder.decodeValue eventDecoder >> toMsg)


eventDecoder : Decoder Event
eventDecoder =
    Decoder.oneOf
        [ eventKind 0 ErrorPopup
            |> field "msg" Decoder.string
        ]


eventKind : Int -> a -> Decoder a
eventKind correctId constructor =
    Decoder.field "kind" Decoder.int |> Decoder.andThen
        ( \id -> if id == correctId
            then
                Decoder.succeed constructor
            else
                Decoder.fail ("Invalid event type: " ++ String.fromInt id)
        )


field : String -> Decoder a -> Decoder (a -> b) -> Decoder b
field name =
    Decoder.field name >> Decoder.map2 (|>)
