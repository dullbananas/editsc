mod block;


use block::Block;
use std::io::{Read, Seek};
use wasm_bindgen_futures::JsFuture;
use web_sys::{File};
use zip::{read::ZipArchive, read::ZipFile, result::ZipError};


pub struct World {
    chunks: Vec<Chunk>,
    project_xml: String,
}


struct Chunk {
    x: i32,
    z: i32,
    //blocks: [[[Block; 16]; 256]; 16],
    blocks: [Block; 16*256*16],
}


#[derive(thiserror::Error, Debug)]
pub enum ImportError {
    #[error("IO error")]
    Io(#[from] std::io::Error),
    #[error("invalid data")]
    Nom,
    #[error("zip extraction failed")]
    Zip(#[from] ZipError),
    #[error("missing data")]
    MissingFile,
}


impl World {
    pub fn new() -> World {
        World {
            chunks: Vec::new(),
            project_xml: String::from(""),
        }
    }


    pub async fn import_file(
        &mut self,
        file: File,
    ) -> Result<(), ImportError> {
        // TODO: use async extraction
        let archive_data = file_bytes(file).await;
        let mut archive = ZipArchive
            ::new(std::io::Cursor::new(archive_data))?;


        //archive.read(get_file("Project.xml")?)?
        get_file(&mut archive, "Project.xml")?
        .read_to_string(&mut self.project_xml)?;

        let chunks_data = {
            let mut chunks_file = get_file(&mut archive, "Chunks32h.dat")?;
            let mut result = vec![0u8; wasm_rs_dbg::dbg!(chunks_file.size() as usize)];
            //archive.read(chunks_file)?
            //chunks_file.read(&mut result[..])?;
            //let mut result = Vec::new();
            //loop {
            let mut ii=0;
            let ll=result.len();
            for mut part in result.chunks_mut(1_000_000) {
                chunks_file.read(&mut part)?;
                std::future::ready(()).await;
                ii+=1_000_000;
                wasm_rs_dbg::dbg!(ii as f32/ll as f32);
            }
            result
        };
        //chunks_reader.read_to_end(&mut chunks_data)?;
        self.chunks = parse_chunks_file(&chunks_data[..])
        .map(|(_, chunks)| chunks)
        .map_err(|_| ImportError::Nom)?;

        Ok(())
    }
}


fn get_file<'a, R: Read + Seek>(
    archive: &'a mut ZipArchive<R>,
    target: &str,
)-> Result<ZipFile<'a>, ImportError> {
    let name: String = archive
        .file_names()
        .find(|name| name.ends_with(target))
        .ok_or(ImportError::MissingFile)?
        // Without this conversion, `name` would reference the archive, which is not allowed while `ZipArchive::by_name` is using a mutable reference of it
        .into();
    Ok(archive.by_name(&name)?)
    /*archive.entries().iter()
        .find(|entry| entry.path.ends_with(name))
        .ok_or(ImportError::MissingFile)*/
    /*let mut names = archive
        .file_names()
        .enumerate();
    while let Some((i, name)) = names.next() {
        if name.ends_with(target) {
            return Ok(archive.by_index(i)?);
        }
    }*/
    /*let names: Vec<_> = archive.file_names().collect();
    for name in names {
        if name.ends_with(target) {
            return Ok(archive.by_name(name)?);
        }
    }
    Err(ImportError::MissingFile)*/
}


async fn file_bytes(file: File) -> Vec<u8> {
    let array_buffer_promise: JsFuture = file
        .array_buffer()
        .into();
    let array_buffer = array_buffer_promise
        .await
        .expect("Could not get ArrayBuffer from file");
    js_sys::Uint8Array
        ::new(&array_buffer)
        .to_vec()
}


fn parse_chunks_file(input: &[u8]) -> nom::IResult<&[u8], Vec<Chunk>> {
    // Ingore the directory (a list of chunks' metadata)
    let (input, _) = nom::bytes::complete::take(65537 * 4 * 3 as usize)(input)?;
    panic!("owoo");

    // Chunks
    let (input, chunks) = nom::multi::many0(|input| {
        // Magic numbers
        let (input, _) = nom::bytes::complete::tag(
            &[0xEF, 0xBE, 0xAD, 0xDE, 0xFE, 0xFF, 0xFF, 0xFF]
        )(input)?;

        // Position
        let (input, x) = nom::number::complete::le_i32(input)?;
        let (input, z) = nom::number::complete::le_i32(input)?;

        // Blocks
        let mut blocks = [Block::from_bits(0); 16*256*16];
        let mut block_index = 0;
        let (input, _) = nom::multi::count(|input| {
            let (input, block_bits) = nom::number::complete::le_u32(input)?;
            blocks
                [(block_index >> 8) & 0x0F]
                [block_index & 0xFF]
                [block_index >> 12]
                = Block::from_bits(block_bits);
            block_index += 1;
            Ok((input, ()))
        }, 16 * 256 * 16)(input)?;

        // Ignore surface points
        let (input, _) = nom::bytes::complete::take(256 * 4 as usize)(input)?;

        Ok((input, Chunk {
            x: 0,
            z: 0,
            blocks: blocks,
        }))
    })(input)?;

    let (input, _) = nom::combinator::eof(input)?;
    Ok((input, chunks))
}
