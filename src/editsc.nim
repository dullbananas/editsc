import asyncjs
import dom
import jsconsole


type
    ElmProgram {.importjs.} = ref object of JsRoot

    EventKind = enum
        ev_import_world = 0,

    Event = ref object of JsRoot
        case kind: EventKind
        of ev_import_world:
            file: dom.File


proc init_elm_program(): ElmProgram {.importjs: "Elm.Editsc.init()".}

proc subscribe(
    program: ElmProgram,
    callback: proc(event: Event),
) {.importjs: "#.ports.rawSend.subscribe(#)".}


var
    elm_program = init_elm_program()
    events: seq[Event] = @[]
    # Called each time an event is added to `events`
    resolve_event: proc() = proc() =
        discard


proc handle_event(event: Event) =
    ## Directly responds to events triggered by Elm
    events.insert(event, i = 0)
    resolve_event()


proc set_event_resolver(resolve: proc()) =
    resolve_event = resolve

proc next_event(): Future[Event] {.async, exportc.} =
    ## Waits for and returns the next event triggered by Elm
    if events.len == 0:
        new_promise(set_event_resolver).await()
    return events.pop()


elm_program.subscribe(handle_event)
