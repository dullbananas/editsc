#![deny(non_snake_case)]
#![deny(unused_must_use)]

mod world;

use crate::world::World;
use wasm_bindgen::{prelude::*, JsCast};
use web_sys::{File};


macro_rules! subtype {
    { $parent:ident -> $child:ident
        kind = $kind:literal,
        $($field:ident: $field_type:ty,)*
    } => {
        #[wasm_bindgen]
        extern "C" {
            pub type $child;

            $(
                #[wasm_bindgen(method, getter)]
                fn $field(this: &$child) -> $field_type;
            )*
        }

        impl $child {
            fn opt_from(x: &$parent) -> Option<&Self> {
                match x.kind() {
                    $kind => Some(x.unchecked_ref()),
                    _ => None,
                }
            }
        }
    };
}


#[wasm_bindgen]
extern "C" {
    type Event;

    #[wasm_bindgen(method, getter)]
    fn kind(this: &Event) -> u8;

    #[wasm_bindgen]
    async fn next_event() -> JsValue;
}


subtype! { Event -> ImportWorld
    kind = 0,
    file: File,
}


// wasm-bindgen doesn't allow `main` as the name here
#[wasm_bindgen]
pub async fn main_please() {
    console_error_panic_hook::set_once();

    let mut world = World::new();

    loop {
        let e: Event = next_event().await.unchecked_into();

        if false {}

        else if let Some(event) = ImportWorld::opt_from(&e) {
            match world.import_file(event.file()).await {
                Ok(()) => todo!(),
                Err(error) => todo!(),
            }
        }

        else {unreachable!()}
    }
}
