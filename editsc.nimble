# Package

version = "0.1.0"
author = "dullbananas"
description = "Survivalcraft world editor"
license = "MIT"
srcDir = "src"
bin = @["editsc"]

backend = "js"

# Dependencies

requires "nim >= 1.0.6"
